Source: dossizola
Section: games
Priority: optional
Maintainer: Yann Dirson <dirson@debian.org>
Build-Depends: libsdl-image1.2-dev, libsdl1.2-dev, docbook-utils, imagemagick, debhelper-compat (= 12)
Standards-Version: 3.9.1
Homepage: https://sourceforge.net/projects/dossizola/
Vcs-Git: https://salsa.debian.org/debian/dossizola.git
Vcs-Browser: https://salsa.debian.org/debian/dossizola

Package: dossizola-data
Architecture: all
Depends: ${misc:Depends}
Recommends: dossizola
Multi-Arch: foreign
Description: Data files for Do'SSi Zo'la game
 Do'SSi Zo'la is an SDL implementation of the Isola board game,
 featuring nice animations, which makes it appealing to children.
 .
 The goal of the basic Isola game is to block the opponent by
 destroying the squares which surround him. In each turn, each player
 must first move to one of the squares adjacent to his current
 position, and then destroy a square of his choice. The first player
 who is unable to move loses.
 .
 This package holds the images used by the Do'SSi Zo'la board game.

Package: dossizola
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, dossizola-data (= ${source:Version})
Description: Isola board game with nice graphics
 Do'SSi Zo'la is an SDL implementation of the Isola board game,
 featuring nice animations, which makes it appealing to children.
 .
 The goal of the basic Isola game is to block the opponent by
 destroying the squares which surround him. In each turn, each player
 must first move to one of the squares adjacent to his current
 position, and then destroy a square of his choice. The first player
 who is unable to move loses.
 .
 Do'SSi Zo'la provides a number of variations of the standard rules.
